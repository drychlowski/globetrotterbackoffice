﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GlobetrotterTravelAgency.Models;
using GlobetrotterTravelAgency.Models.Enums;
using GlobetrotterTravelAgency.Models.HomeViewModels;
using GlobetrotterTravelAgency.Models.Shared;

namespace GlobetrotterTravelAgency.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var departures = new OffersViewModel
            {
                Offers = new List<OfferViewModel>()
                {
                    new OfferViewModel()
                    {
                        Countries = new List<string> {"Tajlandia", "Kambodża"},
                        Title = "Tajlandia i Kambodża",
                        Price = new decimal(4000),
                        SpecialPrice = new decimal(3500),
                        ImageUrl = "content/img/home/pop-dest-asia.jpg",
                        OfferUrl = "/",
                        Type = "Wyprawa",
                        TripPrice = TripPrice.LastMinute,
                        DepartureDate = new DateTime(2018, 12, 16),
                        ReturnDate = new DateTime(2018, 12, 30),
                        Days = 14,
                    },
                    new OfferViewModel()
                    {
                        Countries = new List<string> {"Kambodża"},
                        Title = "Kambodża",
                        Price = new decimal(3000),
                        ImageUrl = "content/img/home/pop-dest-asia1.jpg",
                        OfferUrl = "/",
                        Type = "Wyprawa",
                        TripPrice = TripPrice.LastMinute,
                        DepartureDate = new DateTime(2018, 12, 10),
                        ReturnDate = new DateTime(2018, 12, 17),
                        Days = 7,
                    },
                    new OfferViewModel()
                    {
                        Countries = new List<string> {"RPA"},
                        Title = "RPA",
                        Price = new decimal(5500),
                        SpecialPrice = new decimal(4000),
                        ImageUrl = "content/img/home/pop-dest-africa.jpg",
                        OfferUrl = "/",
                        Type = "Wyprawa",
                        TripPrice = TripPrice.LastMinute,
                        DepartureDate = new DateTime(2018, 12, 10),
                        ReturnDate = new DateTime(2018, 12, 20),
                        Days = 10,
                    },
                    new OfferViewModel()
                    {
                        Countries = new List<string> {"Francja", "Niemcy", "Czechy"},
                        Title = "Paryż, Berlin, Praga",
                        Price = new decimal(3000),
                        ImageUrl = "content/img/home/pop-dest-europe.jpg",
                        OfferUrl = "/",
                        Type = "Wyprawa",
                        TripPrice = TripPrice.LastMinute,
                        DepartureDate = new DateTime(2019, 1, 03),
                        ReturnDate = new DateTime(2019, 1, 10),
                        Days = 7,
                    },
                    new OfferViewModel()
                    {
                        Countries = new List<string> {"Tajlandia"},
                        Title = "Wycieczka po azji",
                        Price = new decimal(4200),
                        ImageUrl = "content/img/home/pop-dest-asia.jpg",
                        OfferUrl = "/",
                        Type = "Wyprawa",
                        TripPrice = TripPrice.LastMinute,
                        DepartureDate = new DateTime(2019, 1, 10),
                        ReturnDate = new DateTime(2019, 1, 20),
                        Days = 10,
                    },
                }
            };

            return View(departures);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
