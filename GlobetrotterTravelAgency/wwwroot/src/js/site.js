$(document).ready(function () {
    new WOW().init();

    jarallax(document.querySelectorAll('.jarallax'));

    jarallax(document.querySelectorAll('.jarallax-keep-img'), {
        keepImg: true
    });

    var swiper = new Swiper('.swiper-container', {
        autoplay: {
            delay: 5000
        },
        breakpoints: {
            // when window width is <= 576
            575: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                //spaceBetween: 10
            },
            // when window width is <= 767
            767: {
                slidesPerView: 2,
                slidesPerGroup: 1,
                //spaceBetween: 10
            },
            // when window width is <= 991
            991: {
                slidesPerView: 2,
                slidesPerGroup: 1,
                //spaceBetween: 20
            },
            // when window width is <= 1199
            1199: {
                slidesPerView: 3,
                slidesPerGroup: 1,
                //spaceBetween: 30
            }
        },
        slidesPerView: 4,
        //spaceBetween: 30,
        slidesPerGroup: 1,
        //slidesOffsetBefore: '24',
        //slidesOffsetAfter: '24',
        navigation: {
            prevEl: '#swiper-prev',
            nextEl: '#swiper-next'
        },
        grabCursor: true
    });
});
