﻿/// <binding ProjectOpened='watch' />
var fs = require('fs'),
    merge = require('merge-stream'),
    gulp = require('gulp'),
    concat = require('gulp-concat'),
    cleanCss = require('gulp-clean-css'),
    babel = require('gulp-babel'),
    sass = require('gulp-sass'),
    npmDist = require('gulp-npm-dist'),
    terser = require('gulp-terser'),
    inject = require('gulp-inject'),
    cache = require('gulp-cache'),
    rename = require('gulp-rename'),
    hash = require('gulp-hash'),
    injectString = require('gulp-inject-string'),
    order = require("gulp-order"),
    clean = require('gulp-clean'),
    rev = require('gulp-rev');
var bundleConfig = JSON.parse(fs.readFileSync('./bundle.json'));

gulp.task('compile:sass', function () {
    return gulp.src('wwwroot/src/sass/**/*.scss')
        .pipe(sass({ includePaths: ['./node_modules']}))
        .pipe(gulp.dest('wwwroot/src/css'));
});

gulp.task('inject:js', function () {
    return gulp.src('Views/Shared/_Layout.cshtml')
        .pipe(inject(gulp.src(['wwwroot/dist/**/*.js', '!wwwroot/dist/**/*.min.js'], { read: false })
            .pipe(order([
                'bundle*.vendor*',
                'bundle*.main*'
            ])), 
            {
                name: 'dev',
                transform: function (filePath) {
                    var newPath = filePath.replace('/wwwroot', '~').replace('@', '@@');
                    return '<script src="' + newPath + '"></script>';
                }
            }))
        .pipe(inject(gulp.src('wwwroot/dist/*.min.js', { read: false })
            .pipe(order([
                'bundle*.vendor*',
                'bundle*.main*'
            ])), 
            {
                transform: function (filePath) {
                    var newPath = filePath.replace('/wwwroot', '~').replace('@', '@@');
                    return '<script src="' + newPath + '"></script>';
                }
            }))
        .pipe(gulp.dest('Views/Shared'));
});

gulp.task('inject:css', function () {
    return gulp.src('Views/Shared/_Layout.cshtml')
        .pipe(inject(
            gulp.src(['wwwroot/dist/**/*.css', '!wwwroot/dist/**/*.min.css'], { read: false })
                .pipe(order([
                    'bundle*.vendor*',
                    'bundle*.main*'
                ])),
            {
                name: 'dev',
                transform: function (filePath) {
                    var newPath = filePath.replace('/wwwroot', '~').replace('@', '@@');
                    return '<link rel="stylesheet" href="' + newPath + '" />';
                }
            }))
        .pipe(inject(
            gulp.src('wwwroot/dist/*.min.css', { read: false })
                .pipe(order([
                    'bundle*.vendor*',
                    'bundle*.main*'
                ])),
            {
                transform: function (filePath) {
                    var newPath = filePath.replace('/wwwroot', '~').replace('@', '@@');
                    return '<link rel="stylesheet" href="' + newPath + '" />';
                }
            }))
        .pipe(gulp.dest('Views/Shared'));
});

gulp.task('bundle-vendor', function () {
    var css = gulp.src(bundleConfig.vendor.styles, { base: '.' })
        .pipe(concat('wwwroot/dist/bundle.vendor.css'))
        .pipe(rev())
        .pipe(gulp.dest('.'))
        .pipe(cache(cleanCss()))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('.'));

    var js = gulp.src(bundleConfig.vendor.scripts, { base: '.' })
        .pipe(concat('wwwroot/dist/bundle.vendor.js'))
        .pipe(rev())
        .pipe(gulp.dest('.'))
        .pipe(cache(terser()))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('.'));

    return merge(css, js);
});

gulp.task('bundle-main:css', function () {
    return gulp.src(bundleConfig.main.styles, { base: '.' })
        .pipe(concat('wwwroot/dist/bundle.main.css'))
        .pipe(rev())
        .pipe(gulp.dest('.'))
        .pipe(cache(cleanCss()))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('.'));
});

gulp.task('bundle-main:js', function () {
    return gulp.src(bundleConfig.main.scripts, { base: '.' })
        .pipe(concat('wwwroot/dist/bundle.main.js'))
        //.pipe(cache(babel({ compact: true, presets: ['@babel/env'] })))
        .pipe(rev())
        .pipe(gulp.dest('.'))
        .pipe(cache(terser()))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('.'));
});


gulp.task('copy:font', function () {
    return gulp.src(bundleConfig.vendor.content.fonts)
        .pipe(gulp.dest('wwwroot/content/font'));
});

gulp.task('clear:cache', function () {
    return cache.clearAll();
});

gulp.task('clear:dist', function () {
    return gulp.src('wwwroot/dist/**/*', { read: false })
        .pipe(clean());
});

gulp.task('clear-css:bundle-main', function () {
    return gulp.src('wwwroot/dist/**/bundle*.main*.css', { read: false })
        .pipe(clean());
});

gulp.task('clear-js:bundle-main', function () {
    return gulp.src('wwwroot/dist/**/bundle*.main*.js', { read: false })
        .pipe(clean());
});

gulp.task('watch', function () {
    gulp.watch('wwwroot/src/sass/**/*', gulp.series('clear-css:bundle-main', 'compile:sass', 'bundle-main:css', 'inject:css'));
    gulp.watch('wwwroot/src/js/**/*', gulp.series('clear-js:bundle-main', 'bundle-main:js', 'inject:js'));
});

gulp.task('copy', gulp.parallel('copy:font'));
gulp.task('bundle-main', gulp.parallel('bundle-main:css', 'bundle-main:js'));
gulp.task('bundle', gulp.series('clear:dist', 'bundle-vendor', 'bundle-main'));
gulp.task('clear', gulp.parallel('clear:cache', 'clear:dist'));
gulp.task('inject', gulp.series('inject:js', 'inject:css'));
gulp.task('build', gulp.series('clear', 'copy', 'compile:sass', 'bundle', 'inject'));
