﻿using System.ComponentModel.DataAnnotations;

namespace GlobetrotterTravelAgency.Models.Shared
{
    public static class PriceType
    {
        public const string Regular = "Regular";
        public const string FirstMinute = "First Minute";
        public const string LastMinute = "Last Minute";
    }
}