﻿namespace GlobetrotterTravelAgency.Models.Enums
{
    public enum TripPrice
    {
        Regular,
        Promo,
        FirstMinute,
        LastMinute
    }
}