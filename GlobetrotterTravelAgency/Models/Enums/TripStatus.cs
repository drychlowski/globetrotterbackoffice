﻿namespace GlobetrotterTravelAgency.Models.Enums
{
    public enum TripStatus
    {
        Booking,
        FullyBooked,
        InProgress,
        Ended
    }
}