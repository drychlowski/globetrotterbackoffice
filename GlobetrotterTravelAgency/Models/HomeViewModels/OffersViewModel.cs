﻿using System.Collections.Generic;

namespace GlobetrotterTravelAgency.Models.HomeViewModels
{
    public class OffersViewModel
    {
        public List<OfferViewModel> Offers { get; set; }
    }
}