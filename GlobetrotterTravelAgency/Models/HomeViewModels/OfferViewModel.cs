﻿using System;
using System.Collections.Generic;
using GlobetrotterTravelAgency.Models.Enums;
using GlobetrotterTravelAgency.Models.Shared;

namespace GlobetrotterTravelAgency.Models.HomeViewModels
{
    public class OfferViewModel
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public List<string> Countries { get; set; }
        public int Days { get; set; }
        public decimal Price { get; set; }
        public decimal? SpecialPrice { get; set; }
        public string ImageUrl { get; set; }
        public string OfferUrl { get; set; }
        public TripPrice TripPrice { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? ReturnDate { get; set; }
    }
}