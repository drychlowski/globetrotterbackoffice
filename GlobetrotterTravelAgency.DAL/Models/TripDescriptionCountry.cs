﻿namespace GlobetrotterTravelAgency.DAL.Models
{
    public class TripDescriptionCountry
    {
        public int TripDescriptionId { get; set; }
        public TripDescription TripDescription { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
    }
}