﻿using System.Collections.Generic;

namespace GlobetrotterTravelAgency.DAL.Models
{
    public class TripDescription
    {
        public int TripDescriptionId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TravelPlan { get; set; }
        public string Accommodation { get; set; }
        public string SpecialNotes { get; set; }
        public int Days { get; set; }
        public ICollection<TripDescriptionCountry> TripDescriptionCountries { get; set; }
    }
}