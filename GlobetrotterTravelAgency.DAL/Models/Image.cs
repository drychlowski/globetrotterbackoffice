﻿namespace GlobetrotterTravelAgency.DAL.Models
{
    public class Image
    {
        public int ImageId { get; set; }
        public string Path { get; set; }
        public string Title { get; set; }
    }
}