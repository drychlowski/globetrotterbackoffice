﻿namespace GlobetrotterTravelAgency.DAL.Models
{
    public class Payment
    {
        public int PaymentId { get; set; }
        public decimal Amount { get; set; }
        public int ReservationId { get; set; }
        public Reservation Reservation { get; set; }
    }
}