﻿using System;

namespace GlobetrotterTravelAgency.DAL.Models
{
    public class Trip
    {
        public int TripId { get; set; }
        public decimal Price { get; set; }
        public int SpecialPrice { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public int MaxPeopleCount { get; set; }
        public int TripDescriptionId { get; set; }
        public TripDescription TripDescription { get; set; }
        public int TripStatusId { get; set; }
        public TripStatus TripStatus { get; set; }
    }
}