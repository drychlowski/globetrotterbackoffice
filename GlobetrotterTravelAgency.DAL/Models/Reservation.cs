﻿using System.Collections.Generic;

namespace GlobetrotterTravelAgency.DAL.Models
{
    public class Reservation
    {
        public int ReservationId { get; set; }
        public int PricePerPerson { get; set; }
        public int TripId { get; set; }
        public Trip Trip { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<ReservationCustomer> ReservationCustomers { get; set; }
    }
}