﻿using Microsoft.AspNetCore.Identity;

namespace GlobetrotterTravelAgency.DAL.Models
{
    public class Role : IdentityRole<int> { }
}