﻿using Microsoft.AspNetCore.Identity;

namespace GlobetrotterTravelAgency.DAL.Models
{
    public class User : IdentityUser<int>
    {
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}