﻿namespace GlobetrotterTravelAgency.DAL.Models
{
    public class TripStatus
    {
        public int TripStatusId { get; set; }
        public string Name { get; set; }
    }
}