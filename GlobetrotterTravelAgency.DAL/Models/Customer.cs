﻿namespace GlobetrotterTravelAgency.DAL.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public int Name { get; set; }
        public int Surname { get; set; }
        public int Pesel { get; set; }
        public int Address { get; set; }
        public int Phone { get; set; }
        public int Mail { get; set; }
        public int PassportNumber { get; set; }
    }
}