﻿namespace GlobetrotterTravelAgency.DAL.Models
{
    public class ReservationCustomer
    {
        public int ReservationId { get; set; }
        public Reservation Reservation { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}